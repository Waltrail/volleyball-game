﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Voleball_Server
{
    public enum Reception
    {
        None,
        High,
        Down
    }
    public class Teammate
    {
        public Vector2 Position;
        public float radius;
        public float strength;
        public float accuracy;
        public float mobility;
        public Reception reception;
        public string name;

        public static List<string> Names = new List<string>()
        {
            "Никита",
            "Олег",
            "Адрей",
            "Дмитрий",
            "Петр",
            "Сергей",
            "Даниил",
            "Артем",
            "Руслан",
            "Кирилл",
            "Александр",
            "Витя",
            "Женя",
            "Игорь",
            "Егор",
            "Серафим",
            "Григорий",
            "Иван",
            "Антон",
            "Тимофей"
        };

        public Teammate(Vector2 position)
        {
            Position = position;
            Random random = new Random();
            radius = (float)random.Next(100, 201) / 100;
            strength = (float)random.Next(50, 151) / 100;
            accuracy = (float)random.Next(50, 151) / 100;
            mobility = (float)random.Next(100, 201) / 100;
            reception = Reception.None;
            name = Names[random.Next(0, Names.Count)];
        }
        public override string ToString()
        {
            return "&mate" + name + "&position" + Position.ToString() + 
                   "&strength" + strength + "&accuracy" + accuracy + 
                   "&mobility" + mobility + "&radius" + radius + "&reception" + reception.ToString()+",";
        }
    }
}
