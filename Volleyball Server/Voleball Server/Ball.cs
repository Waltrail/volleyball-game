﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Voleball_Server
{
    public class BallInfo
    {
        public bool isTouchWeb;
        public bool isTouchPlayer;
        public bool isAut;
        public Vector2 LandPosition;
        public bool isFellInHisField;

        public BallInfo()
        {
            isAut = false;
            isTouchPlayer = false;
            isTouchWeb = false;
            LandPosition = new Vector2();
            isFellInHisField = false;
        }
    }
    public class Ball
    {
        public Vector3 position;
        public Vector3 velosity;
        public float radius;
        public Ball(Vector3 position)
        {
            this.position = position;
            radius = 0.2f;
        }
        public void Set(Vector2 Position, Vector3 Velosity)
        {
            position = new Vector3(Position, 2);
            velosity = Velosity;
        }
        public BallInfo GetNextBourdPosition(Team red, Team blue)
        {
            BallInfo ballInfo = new BallInfo();
            Vector3 pos = position;
            Vector3 vil = velosity;
            while(pos.Z > 0 && !red.IsTouchPlayer(pos) && !blue.IsTouchPlayer(pos) 
               && pos.X > 18 && pos.X < 0 && pos.Y > 0 && pos.Y < 9)
            {
                pos += vil;
                vil.X *= 0.95f;
                vil.Y *= 0.95f;
                vil.Z += 0.1f;
                if(pos.X > 8.95f && pos.X < 9.05 && pos.Z < 2.43f)
                {
                    ballInfo.isTouchWeb = true;
                    break;
                }
            }
            ballInfo.LandPosition = new Vector2(pos.X, pos.Y);
            ballInfo.isFellInHisField = (position.X > 9) ? pos.X > 9 : pos.X < 9;
            ballInfo.isTouchPlayer = red.IsTouchPlayer(this) || blue.IsTouchPlayer(this);
            ballInfo.isAut = pos.X > 18 && pos.X < 0 && pos.Y > 0 && pos.Y < 9;
            position = pos;
            return ballInfo;
        }
    }
}
