﻿using Fleck;
using System;
using System.Collections.Generic;
using System.Text;

namespace Voleball_Server
{
    public class Player
    {
        public string Name;
        public IWebSocketConnection Socket;

        public Player(string name, IWebSocketConnection socket)
        {
            Name = name;
            Socket = socket;
        }
    }
}
