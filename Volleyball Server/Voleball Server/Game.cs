﻿using Fleck;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Voleball_Server
{
    public enum turn
    {
        RedTurn,
        BlueTurn
    };

    public class Game
    {
        public Player Red;
        public Player Blue;
        public Ball Ball;
        public Team RedTeam;
        public Team BlueTeam;
        private Random random;
        public turn Turn;

        public List<Vector2> redPositions;
        public List<Vector2> bluePositions;
        public Game(Player red, Player blue)
        {
            Red = red;
            Blue = blue;
            Program.lobby.Remove(red);
            Program.lobby.Remove(blue);
            Console.WriteLine("Players added to game and removed from lobby");
            random = new Random();
            Turn = random.Next(0, 1) == 0 ? turn.RedTurn : turn.BlueTurn;
            Red.Socket.OnMessage += onred;
            Blue.Socket.OnMessage += onblue;
            redPositions = new List<Vector2>()
            {
                new Vector2(1, 8),
                new Vector2(8, 8),
                new Vector2(8, 4.5f),
                new Vector2(8, 1),
                new Vector2(1, 1),
                new Vector2(1, 4.5f)
            };
            bluePositions = new List<Vector2>()
            {
                new Vector2(17, 1),
                new Vector2(10, 1),
                new Vector2(10, 4.5f),
                new Vector2(10, 8),
                new Vector2(17, 8),
                new Vector2(17, 4.5f)
            };
            RedTeam = new Team(redPositions,0);
            BlueTeam = new Team(bluePositions,1);
            if (Turn == turn.BlueTurn)
                Ball = new Ball(new Vector3(bluePositions[0], 2));
            else
                Ball = new Ball(new Vector3(redPositions[0], 2));
            red.Socket.Send("GameStartRed" + Turn.ToString());
            blue.Socket.Send("GameStartBlu"+ Turn.ToString());
        }

        private void onred(string obj)
        {

        }

        private void onblue(string obj)
        {
            
        }
        public bool isInGame(IWebSocketConnection player)
        {
            return player == Red.Socket || player == Blue.Socket;               
        }
        string GetState()
        {
            return "";
        }

        void SetState(string state)
        {

        }
        public void Update()
        {
            
        }
        public void GameOver()
        {
            Red.Socket.OnMessage -= onred;
            Blue.Socket.OnMessage -= onblue;
            Program.lobby.Add(Red);
            Program.lobby.Add(Blue);
            Program.games.Remove(this);
        }        

        public void Stop()
        {
            Red.Socket.Send("gamedisconnect");
            Blue.Socket.Send("gamedisconnect");
            GameOver();
        }
    }
}
