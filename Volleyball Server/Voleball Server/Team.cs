﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Voleball_Server
{
    public class Team
    {
        public List<Teammate> players;
        private List<Vector2> Positions;
        public int direction;
        public Team(List<Vector2> positions, int Direction)
        {
            Positions = positions;
            direction = Direction;
            players = new List<Teammate>();
            for(int i = 0; i < 6; i++)
            {
                players.Add(new Teammate(Positions[i]));
            }
        }
        public void Rotate()
        {
            Teammate temp = players[0];
            players.RemoveAt(0);
            players.Add(temp);
            for (int i = 0; i < players.Count; i++)
            {
                players[i].Position = Positions[i];
            }
        }
        public string GetTeamInfo()
        {
            string info = "&team";
            foreach(var player in players)
            {
                info += player.ToString();
            }
            return info;
        }

        internal bool IsTouchPlayer(Ball ball)
        {
            foreach(var player in players)
            {
                if(ball.position.Z <= 2)
                {
                    if (player.reception == Reception.Down)
                    {
                        if (direction == 0) {
                            if ()
                            {

                            }
                        }
                    }
                    else if(player.reception == Reception.High)
                    {

                    }
                }
            }
            return false;
        }
        public static bool isInsideCircle(Vector2 position, float radius)
        {

            return false;
        }
    }
}
