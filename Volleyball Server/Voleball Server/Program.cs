﻿using System;
using System.Collections;
using System.Timers;
using System.Net.WebSockets;
using Fleck;
using System.Json;
using System.Collections.Generic;

namespace Voleball_Server
{
    class Program
    {
        const int GAMEPLAYERS = 2;
        internal static List<Player> lobby;
        public static List<Game> games;
        static void Main(string[] args)
        {
            var id = 1000;    // I give every ship an ID - 
                              // the own ship on every client has ID = 1
            var ticks = 0;    // I will also record game time
            string adress = "";
            Console.WriteLine("Write ip and port (default: ws://0.0.0.0:8080)");
            adress = Console.ReadLine();
            if(adress == "")
                adress = "ws://0.0.0.0:8080";
            var allSockets = new Hashtable();    // Stores the connections with ship-IDs
            var server = new WebSocketServer(adress);    //Creates the server
            var timer = new Timer(40);    // This is quite close the interval in JavaScript
            bool isPlaying = false;
            bool isGameStart = false;
            int PlayersConnected = 0;
            lobby = new List<Player>();
            games = new List<Game>();
            server.Start(socket =>
            {
                socket.OnOpen = () =>
                {
                    //Code what happens when a client connects to the server
                    Console.WriteLine("Connection");
                    PlayersConnected++;
                    Console.WriteLine("Connected players: " + PlayersConnected);
                    Console.WriteLine("Players in lobby: " + lobby.Count);                    
                };

                socket.OnClose = () =>
                {
                    //Code what happens when a client disconnects from the server
                    Console.WriteLine("Player disconnect");
                    PlayersConnected--;
                    for(int i = 0; i < games.Count; i++)
                    {
                        if (games[i].isInGame(socket))
                        {
                            games[i].Stop();
                            i--;
                        }
                    }
                    for(int i =0; i <lobby.Count;i++)
                    {
                        if(lobby[i].Socket == socket)
                        {
                            lobby.Remove(lobby[i]);
                            i--;
                        }
                    }
                    tryagain:
                    try
                    {
                        foreach (Player player in lobby)
                        {
                            string Lobby = "lobby";
                            foreach (Player pplayer in lobby)
                            {
                                if (pplayer.Socket != player.Socket)
                                    Lobby += pplayer.Name + ",";
                            }
                            player.Socket.Send(Lobby);
                            player.Socket.Send("index" + lobby.IndexOf(player));
                        }
                    }
                    catch
                    {
                        goto tryagain;
                    }
                    Console.WriteLine("Connected players: " + PlayersConnected);
                    Console.WriteLine("Players in lobby: " + lobby.Count);                    
                };
                socket.OnMessage = message =>
                {
                    if (message.Contains("newuser"))
                    {

                        Console.WriteLine("Message");
                        Console.WriteLine(message);
                        lobby.Add(new Player(message.Substring(7), socket));
                        socket.Send("JustJoined");

                        tryagain:
                        try
                        {
                            foreach (Player player in lobby)
                            {
                                string Lobby = "lobby";
                                foreach (Player pplayer in lobby)
                                {
                                    if (pplayer.Socket != player.Socket)
                                        Lobby += pplayer.Name + ",";
                                }
                                player.Socket.Send(Lobby);
                                player.Socket.Send("index" + lobby.IndexOf(player));
                            }
                        }
                        catch
                        {
                            goto tryagain;
                        }
                    }
                    if (message.Contains("inviteto") && message.Contains("F"))
                    {
                        int invitedPlayer = -1;
                        int from = message.IndexOf('F');
                        int invitedFrom = -1;
                        if (int.TryParse(message.Substring(from + 1), out invitedFrom))
                        {
                            if (int.TryParse(message.Substring(8, from - 8), out invitedPlayer))
                            {
                                if (invitedPlayer >= 0 && invitedPlayer < lobby.Count && invitedFrom >= 0 && invitedFrom < lobby.Count)
                                {
                                    Console.WriteLine("invite from:" + invitedFrom + ", to:" + invitedPlayer);
                                    lobby[invitedPlayer].Socket.Send("invitefrom"+invitedFrom+"P"+lobby[invitedFrom].Name);
                                }
                            }
                        }                        
                    }
                    if (message.Contains("accept"))
                    {
                        int invitedPlayer = -1;
                        int from = message.IndexOf('F');
                        int invitedFrom = -1;
                        if (int.TryParse(message.Substring(from + 1), out invitedFrom))
                        {
                            if (int.TryParse(message.Substring(6, from - 6), out invitedPlayer))
                            {
                                if (invitedPlayer >= 0 && invitedPlayer < lobby.Count && invitedFrom >= 0 && invitedFrom < lobby.Count)
                                {
                                    Console.WriteLine("accept from:" + invitedFrom + ", to:" + invitedPlayer);
                                    if (games.Count == 0)
                                    {
                                        timer.Start();
                                    }
                                    games.Add(new Game(lobby[invitedFrom], lobby[invitedPlayer]));
                                }
                            }
                        }
                        foreach (Player player in lobby)
                        {
                            string Lobby = "lobby";
                            foreach (Player pplayer in lobby)
                            {
                                if (pplayer.Socket != player.Socket)
                                    Lobby += pplayer.Name + ",";
                            }
                            player.Socket.Send(Lobby);
                            player.Socket.Send("index" + lobby.IndexOf(player));
                        }
                    }
                    Console.WriteLine("Connected players: " + PlayersConnected);
                    Console.WriteLine("Players in lobby: " + lobby.Count);
                };
            });

            //Server side logic
            timer.Elapsed += new ElapsedEventHandler((sender, e) => //This callback is 
                                                                    //used to generate objects like Asteroids
            {
                //Here the server executes some logic and sends the result to all 
                //connected clients
                if(games.Count > 0)
                {
                    foreach(var game in games)
                    {
                        game.Update();
                    }
                }
            });

            // Here we close everything
            var input = Console.ReadLine();
            timer.Close();
            timer.Dispose();
            server.Dispose();
        }
    }
}
