var isConnected = false;
var team = -1;
var socket;
var Text = '';
var aspect;
var bourd;
var displayLogin = false;
var lobbyList;
var clientNumber = -1;
var timer = 120;
var gamestate = 0;
var turn = -1;
var socketSetup = function() {
	if (typeof WebSocket !== 'undefined') {
		//If the type of WebSocket is
		//known then they are active
		socket = new WebSocket('ws://0.0.0.0:8080'); //So let us create an
		//instance of them!
		socket.onerror = () => {
			console.log('Connection error');
			isConnected = false;
			document.getElementById('connecting').style.display = 'none';
			document.getElementById('connectfail').style.display = 'flex';
			document.getElementById('login').style.display = 'none';
			document.getElementById('lobby').style.display = 'none';
		};
		socket.onopen = function() {
			//What happens if we can now really connect?!
			multiPlayer = true; //We are obviously in multiplayer mode
			document.title = 'Login to'; //And I do want to see this -
			//so I'll make a title change
			document.getElementById('connectfail').style.display = 'none';
			document.getElementById('connecting').style.display = 'none';
			document.getElementById('login').style.display = 'flex';
			document.getElementById('lobby').style.display = 'none';
		};

		socket.onclose = function() {
			console.log('Connection error');
			isConnected = false;
			document.getElementById('connecting').style.display = 'none';
			document.getElementById('connectfail').style.display = 'flex';
			document.getElementById('login').style.display = 'none';
			document.getElementById('lobby').style.display = 'none';
		};

		socket.onmessage = function(e) {
			// A message from the server!
			//var obj = JSON.parse(e.data);     //Parse it - so that we can handle the data
			console.log(e.data);
			Text = e.data;
			if (e.data == 'JustJoined') {
				displayLogin = true;
				document.getElementById('login').style.display = 'none';
				document.getElementById('lobby').style.display = 'flex';
			}
			if (e.data.slice(0, 5) == 'lobby') {
				users = e.data.slice(5);
				update(users);
			}
			if (e.data.slice(0, 5) == 'index') {
				clientNumber = parseInt(e.data.slice(5));
			}
			if (e.data.slice(0, 10) == 'invitefrom') {
				var p = e.data.indexOf('P');
				console.log('Indited from ' + e.data.slice(10, p) + ', ' + e.data.slice(p + 1) + ', p=' + p);
				invited(parseInt(e.data.slice(10, p)), e.data.slice(p + 1));
			}
			if(e.data.slice(0,9) == 'GameStart'){	
				console.log("GameStart");
				document.getElementById('lobby').style.display = 'none';
				if(e.data.slice(10,13) == "Red")
					team = 0;
				else{
					team = 1;
				}
				if(e.data.slice(14)=="RedTurn" && team == 0){
					turn = 1;
				}		
				else if(e.data.slice(14) == "BlueTurn" && team == 1){
					turn = 1;
				}
				else{
					turn = 0;
				}
				document.getElementById('connectfail').style.display = 'none';
				document.getElementById('connecting').style.display = 'none';
				document.getElementById('login').style.display = 'none';				
			}
			if(e.data.slice(0,14) == "gamedisconnect"){
				alert("Игрок отключился от игры");
			}
		};
	} else gameRunning = true; //Single player games will always be started instantly
};
retry = () => {
	try {
		document.getElementById('connectfail').style.display = 'none';
		document.getElementById('connecting').style.display = 'flex';
		document.getElementById('login').style.display = 'none';
		document.getElementById('lobby').style.display = 'none';
		socketSetup();
		isConnected = true;
	} catch (error) {
		isConnected = false;
		document.getElementById('connecting').style.display = 'none';
		document.getElementById('connectfail').style.display = 'flex';
		document.getElementById('login').style.display = 'none';
		document.getElementById('lobby').style.display = 'none';
	}
};

function isNullOrWhiteSpace(str) {
	return !str || str.length === 0 || /^\s*$/.test(str);
}

function loginto() {
	console.log('Trying to login');
	name = '' + document.getElementById('Name').value;
	if (!isNullOrWhiteSpace(name)) {
		displayLogin = false;
		document.getElementById('login').style.display = 'none';
		socket.send('newuser' + name);
	} else {
		alert('Uncorrected name');
	}
}
document.addEventListener('DOMContentLoaded', () => {
	retry();
	document.getElementById('Name').addEventListener('keydown', function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			loginto();
		}
	});
});
invite = (index, button) => {
	console.log('SEND INVITE TO ' + index);
	inviteto = 'inviteto';
	if (index >= clientNumber) {
		index++;
	}
	console.log(button);
	button.value = 'Отправлено';
	button.textContent = 'Отправлено';
	timer = 0;
	inviteto += index + 'F' + clientNumber;
	socket.send(inviteto);
};
invited = (from, Name) => {
	console.log(from + ', ' + Name);
	lobby = document.getElementById('lobby');
	lobby.innerHTML +=
		"<div id='invite'><p>Приглашение от " +
		Name +
		"</p><div id='invitebuttons'><button onclick='inviteAccept(true," +
		from +
		")'>Принять</button><button onclick='inviteAccept(false," +
		from +
		")'>Игнорировать</button></div></div>";
};
inviteAccept = (state, from) => {
	if (state) {
		socket.send('accept' + clientNumber + 'F' + from);
	}
	lobby = document.getElementById('lobby');
	lobby.removeChild(lobby.lastChild);
};
update = (users) => {
	lobby = document.getElementById('lobby');
	lobby.style.display = 'flex';
	lobby.innerHTML = '<p>Players online</p>';
	user = '';
	index = 0;
	for (let i = 0; i < users.length; i++) {
		if (users[i] == ',') {
			lobby.innerHTML +=
				"<div class='player'><p class='playerName'>" +
				user +
				"</p><button onclick='invite(" +
				index +
				", this)'>Пригласить</button></div>";
			user = '';
			index++;
		} else {
			user += users[i];
		}
	}
	if (index == 0) {
		lobby.innerHTML += '<p>currently noone</p>';
	}
};
var canvas;

function setup() {
	canvas = createCanvas(windowWidth, windowHeight);
	frameRate(30);
	fill(245, 123, 158);
	textSize(50);
	bourd = loadImage('content/bourd.png');
}

function draw() {
	if (timer < 120) {
		timer++;
	}
	canvas.width = windowWidth;
	canvas.height = windowHeight;
	if (timer == 119) {
		var playerlist = document.getElementsByClassName('player');
		for (let i = 0; i < playerlist.length; i++) {
			playerlist.item(i).lastChild.value = 'Пригласить';
			playerlist.item(i).lastChild.textContent = 'Пригласить';
		}
	}
	aspect = height / bourd.height;
	background(200);
	image(bourd, 10, 10, bourd.width * aspect, bourd.height * aspect);
	text(key, 33, 65);
	text(Text, 233, 65);
	if (keyIsPressed) {
		socket.send(key);
	}
}
